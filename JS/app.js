let icons = document.querySelectorAll('.icon-password');
console.log(icons);

icons.forEach(icon => {
    icon.addEventListener('click', () => {
        icon.classList.toggle('fa-eye-slash')
        const input = icon.previousElementSibling
        if (input.type === 'password') {
            input.type = 'text'
        }
        else { input.type = 'password' }
    })
});


const form = document.querySelector('.password-form');
const error = form.querySelector('.error')
form.addEventListener('submit', (event) => {
    event.preventDefault()
    if (form.password.value === form.confirmPassword.value) {
        alert('You are welcome');
        error.textContent = ''
    } else {
        error.textContent = 'Потрібно ввести однакові значення';
    }
});
